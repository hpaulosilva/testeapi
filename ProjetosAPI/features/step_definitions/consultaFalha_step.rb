Quando("realizar uma requisiçao de consulta com cep invalido") do
  @response = HTTParty.get("https://viacep.com.br/ws/0000000/json/")
end

Entao("a api me retornar os dados de consulta erro com status") do
  puts "Status: #{@response.code}"
end
